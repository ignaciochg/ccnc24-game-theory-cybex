pkg load symbolic

syms

% system variables set by system admin
% tune game here
number_of_players = 2;
system_start_value = 0;
added_value_modifier = 1.5;
% pays
incentive_pay = 1;
incentive_a = 1; % must be >1
value_pay = 5; # system pay multiplier
# costs
entry_price = 0; 
cost_utility_per_quality_unit = 1;
# probabilities and percentages
leak_percentage = 0.1;
attack_risk = [0.01, 0.01]

# player inputs vector where 
# 0 <= quality_of_data <= 1
quality_of_data = [0.05,0.02]





##incentive_function
incentive_func = (quality_of_data)./(quality_of_data .+ incentive_a)
incentive_payout =  incentive_func .*incentive_pay

average_quality = sum(quality_of_data)/number_of_players

# percentage of the databse leaked
##leak_percentage_of_player = [0.1, 0.1]
##lp = sum(leak_percentage_of_player)

added_value = quality_of_data .+ added_value_modifier
system_added_value = sum(added_value)/number_of_players

system_value = system_start_value + system_added_value

system_pay = (added_value./system_value).*(value_pay/number_of_players).*(added_value/average_quality)

risk_cost = attack_risk .* cost_utility_per_quality_unit .* leak_percentage .*(added_value./system_value)


utility = system_pay .+ incentive_payout .-risk_cost .- entry_price

% plot a function
####clf;
##fplot (@cos, [0, 2*pi]);
% fplot (@sin, [-10, 10]);
#title ("fplot() single function");

% map function x^2 to vector
% arrayfun(@(x) x^2, [0, 1; 2, 3])

##for i = [1,3;2,4]
##  i
##endfor

##function system_added_value = sav (v)
##  system_added_value = 0;
##  if (is_vector (v))
##    system_added_value = sum (v);
##  else
##    error ("avg: expecting vector argument");
##  endif
##endfunction

##function quality_of_data = qod (v)
##  quality_of_data = 0;
##  if (is_vector (v))
##    quality_of_data = sum (v);
##  else
##    error ("avg: expecting vector argument");
##  endif
##endfunction