
clc % clear output terminal

pkg load symbolic

syms q1 q2 

# q1 = Q1, q2 = Q2



syms E A C L I V a


u1 = -E -(2*A*C*L*q1/(q1+q2))+(I*q1/(q1+a))+((V*(q1^2)*2)/((q1+q2)^2))
u2 = -E -(2*A*C*L*q2/(q1+q2))+(I*q2/(q2+a))+((V*(q2^2)*2)/((q1+q2)^2))

difu1 = diff(u1, q1) 
difu2 = diff(u2, q2) 
dif2u1 = diff(difu1, q1) 
dif2u2 = diff(difu2, q2) 

##du1h = function_handle(difu1)
##du2h = function_handle(difu2)
##d2u1h = function_handle(dif2u1)
##d2u2h = function_handle(dif2u2)

##du1dq1 = (-2*A*C*L*q2/((q1+q2)^2))+(I*a/((q1+a)^2))+((4*V*q1*q2)/((q1*q2)^3))
##
##du2dq2 = (-2*A*C*L*q1/((q1+q2)^2))+(I*a/((q2+a)^2))+((4*V*q1*q2)/((q1*q2)^3))
##
##d1h = function_handle(du1dq1)
##d2h = function_handle(du2dq2)

##disp("solving for q1 and q2...")
##[Q1_star,Q2_star] = solve(difu1h == 0,  difu2 == 0, q1,q2 )

##r1 = 
##r2 = 


##q1 = 0:0.01:1
##q2 = 0:0.01:1
##
##E = 0.5;
##A = 0.005;
##C = 5;
##L = 0.001;
##I = 50;
##V = 5;
##a = 100;
##
##
##
##[xx,yy]=meshgrid(q1,q2);
####du2h(A, C, I, L, V, a, q1, q2)
##
##r1 = du1h(A, C, I, L, V, a, xx, yy)
##r2 = du2h(A, C, I, L, V, a, xx, yy)
####
##
##r3 = d2u1h(A, C, I, L, V, a, q1, q2)
####r = r1
####meshc(xx,yy, r)
##
##figure (1);
##meshc(xx,yy, r1);
##title ("Player 1 Utility");
##xlabel ("Quality Player 1");
##ylabel ("Quality Player 2");
##
##figure (2);
##
##meshc(xx,yy, r2)
##title ("Player 2 Utility");
##xlabel ("Quality Player 1");
##ylabel ("Quality Player 2");