##pkg load linear-algebra
##
##
##poss = cartprod(0:0.01:1,0:0.01:1)



####text (pi, 0.7, "arbitrary text");
##legend ("x, x/y", "y, x/y");

x = 0:1:500
y = 0:0.01:1



[xx,yy]=meshgrid(x,y);
rr = (100.*xx .* log(yy+1))./(xx.+100)

meshc(xx,yy, rr)

title ("Q(D)/averge(Q)");
xlabel ("# samples");
ylabel ("Quality");