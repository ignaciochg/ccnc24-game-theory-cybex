##pkg load linear-algebra
##
##
##poss = cartprod(0:0.01:1,0:0.01:1)

quality = 0:0.01:1
other_average = 0:0.01:1


r = quality .* other_average

plot(quality,quality,
     other_average,other_average,
     quality,r, other_average, r)

##

####text (pi, 0.7, "arbitrary text");
##legend ("x, x/y", "y, x/y");


[xx,yy]=meshgrid(quality,other_average);
meshc(xx,yy, xx .* yy)

title ("Q(D)/averge(Q)");
xlabel ("Quality %");
ylabel ("Average Quality");