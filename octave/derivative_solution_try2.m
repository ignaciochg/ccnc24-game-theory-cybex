
use_log_incentive = 1;
run_simulation = 1;
run_optimal_calculation = 0;
show_graph_bar = 1;




clc % clear output terminal

pkg load symbolic

syms q1 q2 

# q1 = Q1, q2 = Q2



syms Ce Pra Lp Cq St Ip Vp a

av_modifier = 1.5;
numb_players = 2;

system_value = St + ((q1*av_modifier + q2*av_modifier)/numb_players)
average_q = (q1 + q2) / numb_players

if use_log_incentive
  pkg load linear-algebra
  incentive1 = (log(q1+1)*Ip)/a
  incentive2 = (log(q2+1)*Ip)/a
else
  incentive1 = ((q1*Ip)/(q1+a))
  incentive2 = ((q2*Ip)/(q2+a))
endif 

risk_cost1 = ((Pra*Lp*Cq*av_modifier*q1)/(system_value))
risk_cost2 = ((Pra*Lp*Cq*av_modifier*q2)/(system_value))

system_pay1 = (Vp/numb_players)*(q1*av_modifier/system_value)*(q1/average_q)
system_pay2 = (Vp/numb_players)*(q2*av_modifier/system_value)*(q2/average_q)

u1 = -Ce - risk_cost1 + incentive1 + system_pay1
u2 = -Ce - risk_cost2 + incentive2 + system_pay2

difu1 = diff(u1, q1) 
difu2 = diff(u2, q2) 
dif2u1 = diff(difu1, q1)
dif2u2 = diff(difu2, q2) 

fu1h = function_handle(u1)
fu2h = function_handle(u2)
du1h = function_handle(difu1)
du2h = function_handle(difu2)
d2u1h = function_handle(dif2u1)
d2u2h = function_handle(dif2u2)

if run_optimal_calculation
  disp("solving for q1 and q2...")
  [Q1_star,Q2_star] = solve(difu1h == 0,  difu2 == 0, q1,q2 )
endif

if run_simulation
  
  resolution = 0.01
  
  q1 = 0:resolution:1
  q2 = 0:resolution:1
  
  Ce = 60;
  Pra = 0.005;
  Cq = 60;
  Lp = 0.001;
  Ip = 50;
  Vp = 5;
  a = 1; # a > 0
  St = 5;
  
  
  [xx,yy]=meshgrid(q1,q2);
  ####du2h(A, C, I, L, V, a, q1, q2)
  ##
  r1 = fu1h(Ce, Cq, Ip, Lp, Pra, St, Vp, a, xx, yy)
  r2 = fu2h(Ce, Cq, Ip, Lp, Pra, St, Vp, a, xx, yy)
  r3 = du1h(Cq, Ip, Lp, Pra, St, Vp, a, xx, yy)
  r4 = du2h(Cq, Ip, Lp, Pra, St, Vp, a, xx, yy)
  ####
  ##
  ##r5 = d2u1h(A, C, I, L, V, a, q1, q2)
  ####r = r1
  ####meshc(xx,yy, r)
  ##
  figure (1,"name","Player 1 Utility");
  meshc(xx,yy, r1);
  title ("Player 1 Utility");
  xlabel ("Quality Player 1");
  ylabel ("Quality Player 2");
  if show_graph_bar
    colorbar()
  endif
  
  figure (2,"name","Player 2 Utility");
  meshc(xx,yy, r2)
  title ("Player 2 Utility");
  xlabel ("Quality Player 1");
  ylabel ("Quality Player 2");
  if show_graph_bar
    colorbar()
  endif
  
  figure (3,"name","du1/dq1");
  meshc(xx,yy, r3);
  title ("du1/dq1");
  xlabel ("Quality Player 1");
  ylabel ("Quality Player 2");
  if show_graph_bar
    colorbar()
  endif 
 
  figure (4,"name","du2/dq2");
  meshc(xx,yy, r4)
  title ("du2/dq2");
  xlabel ("Quality Player 1");
  ylabel ("Quality Player 2");
##  annotation ("arbitrary text",0,1);
##  annotation('textbox',[0.875 0.1 0.1 0.1],'string','my text','edgecolor','k','linewidth',1,'fitboxtotext','off');
##  annotation('textbox',[0.875 0.1 0.1 0.1],'string','my text','edgecolor','k','linewidth',1,'fitboxtotext','off');

  if show_graph_bar
    colorbar()
  endif
 endif