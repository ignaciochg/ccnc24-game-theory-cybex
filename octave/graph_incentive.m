##pkg load linear-algebra
##
##
##poss = cartprod(0:0.01:1,0:0.01:1)


clc % clear output terminal

pkg load symbolic
pkg load linear-algebra

syms q1 q2 
syms Ce Pra Lp Cq St Ip Vp a

####text (pi, 0.7, "arbitrary text");
##legend ("x, x/y", "y, x/y");





incentive1_log = (log(q1+1)*Ip)/a
incentive1 = ((q1*Ip)/(q1+a))


i1hl = function_handle(incentive1_log)
i1h = function_handle(incentive1)

Ip = 1.43;
a = 1;
x = 0:0.01:1;

y = i1h(Ip, a, x);
y_log = i1hl(Ip, a, x);

plot(x,y,"-",x,y_log,"-");

title ("Inventive functions");
xlabel ("Quality %");
ylabel ("Incentive");

legend("(Ip*x)/(x+a)", "log(x+1)/a")