

pkg load symbolic 

syms x y

a = -y+3
b = 3*x+y

[X, Y] = solve(x == a, b == -3)
##[X, Y] = solve(x^2 == 4, x + y == 1, x, y)
