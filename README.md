


# Game Theory for Privacy-Preserving Cybersecurity Information Exchange Framework

## Citation

If you use this code in your work, please cite our paper using the following BibTeX entry:

```bibtex
@INPROCEEDINGS{Asta2401:Game,
AUTHOR="Ignacio Astaburuaga and Shamik Sengupta",
TITLE="Game Theory for {Privacy-Preserving} Cybersecurity Information Exchange
Framework",
BOOKTITLE="2024 IEEE 21st Consumer Communications \& Networking Conference (CCNC)
(CCNC 2024)",
ADDRESS="Las Vegas, USA",
PAGES=6,
DAYS=6,
MONTH=jan,
YEAR=2024,
KEYWORDS="Game Theory; Cybersecurity; Cyber Threat Intelligence (CTI); Security and
privacy; Intelligence sharing",
ABSTRACT="Analyzing the intricate dynamics of cyber threat information (CTI) sharing
platforms promotes strategic data exchange, fortifying organizations with
the collective strength needed to mitigate cyber threats effectively. This
research aims to quantitatively assess the utility of joining a cyber
threat intelligence platform. More specifically, this paper attempts to
apply the principles of game theory to cyber threat intelligence platforms
to uncover how the game develops over time and examine whether it is
beneficial for an organization to join in the first place."
}
```
## Usage

To run this project, you need to have [GNU Octave](https://www.gnu.org/software/octave/) installed on your system. You can download and install it from the official [GNU Octave website](https://www.gnu.org/software/octave/).

